import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000], # list of pdfs ids for which all variations (error sets) will be included as weights
    'alternative_pdfs':None, # list of pdfs ids for which only the central set will be included as weights
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}
from MadGraphControl.MadGraphUtils import *
import os
import subprocess
import math
import shutil

process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~ 
output -f
"""
process_dir = new_process(process)


# Run card
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

settings = {'nevents'        :10000,
            'beamEnergy'     :beamEnergy }
modify_run_card(process_dir = process_dir,runArgs = runArgs,settings = settings)

# Parameters card
paramToCopy = 'aMcAtNlo_param_card_tt.dat'
paramDestination = process_dir+'/Cards/param_card.dat'
paramfile        = subprocess.Popen(['get_files','-data',paramToCopy])
paramfile.wait()
shutil.copy(paramToCopy,paramDestination)

# MadSpin card
os.makedirs(process_dir+'/madspin')
madspin_card = process_dir+'/Cards/madspin_card.dat'
mscard = open(madspin_card,'w') 
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed 21
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
# running the actual code
launch
""")
mscard.close()

# Generation of events
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
evgenConfig.description = "pp_ttbar"
evgenConfig.generators = ["MadGraph","Pythia8"]
evgenConfig.keywords = ["SM","ttbar","jets"]
evgenConfig.nEventsPerJob = 5000


### Shower 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

### Event filter
# include("GeneratorFilters/TTbarWToLeptonFilter.py")
# filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
# filtSeq.TTbarWToLeptonFilter.Ptcut = 0.0