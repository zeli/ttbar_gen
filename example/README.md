setupATLAS

asetup 21.6.63,AthGeneration

Gen_tf.py  --jobConfig=./ --outputEVNTFile=./tmp.root

asetup 21.2,AthDerivation,latest

Reco_tf.py --inputEVNTFile ./tmp.root --outputDAODFile test.root --reductionConf TRUTH1 

//GRID job

source run*.sh